;;; packages.el --- AMPL layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author:  <tref@questor>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Code:
(defconst ampl-packages
  '(
    company              ;; Autocompletion
    hs-minor-mode        ;; Folding brackets
    smartparens          ;; Highlight parentheses
    (ampl-mode :location (recipe :fetcher github
                                 :repo "dpo/ampl-mode"
                                 :files ("emacs/ampl-mode.el")))))


(defconst reserved-keywords '("Current" "complements" "integer" "solve_result_num" "IN" "contains" "less" "suffix" "INOUT" "default" "logical" "sum" "Infinity" "dimen" "max" "symbolic" "Initial" "div" "min" "table" "LOCAL" "else" "option" "then" "OUT" "environ" "setof" "union" "all" "exists" "shell_exitcode" "until" "binary" "forall" "solve_exitcode" "while" "by" "if" "solve_message" "within" "check" "in" "solve_result"))

(defconst function-keywords '("abs" "acos" "acosh" "alias" "asin" "asinh" "atan" "atan2" "atanh" "ceil" "cos" "ctime" "ctime" "exp" "floor" "log" "log10" "max" "min" "precision" "round" "sin" "sinh" "sqrt" "tan" "tanh" "time" "trunc"))


(defconst ampl-completions
  (append reserved-keywords function-keywords))

(defun company-ampl (command &optional arg &rest ignored)
  "Company backend for the AMPL language. COMMAND ARG IGNORED."
  (interactive (list 'interactive))

  (case command
    (interactive (company-begin-backend 'company-ampl))
    (prefix (and (eq major-mode 'ampl-mode)
                 (company-grab-symbol)))
    (candidates
     (remove-if-not
      (lambda (c) (string-prefix-p arg c))
      ampl-completions))))

(defun ampl/init-ampl-mode ()
  (use-package ampl-mode
    :mode "\\.\\(mod\\|dat\\|run\\)\\'"
    :init (add-to-list 'company-backends-ampl-mode
                       '(company-ampl company-capf company-dabbrev-code))
    :config
    ;; "`*' is not part of variable names, commands, etc.
    (modify-syntax-entry ?* "." ampl-mode-syntax-table)
    ;; comment style “/* … */”
    (modify-syntax-entry ?/  ". 14b" ampl-mode-syntax-table)
    (modify-syntax-entry ?*  ". 23b"   ampl-mode-syntax-table)
    (spacemacs/set-leader-keys-for-major-mode 'ampl-mode "ss" 'ampl-repl)
    (spacemacs/set-leader-keys-for-major-mode 'ampl-mode "sf" 'ampl-send-file)
    (spacemacs/set-leader-keys-for-major-mode 'ampl-mode "sr" 'ampl-send-region)
  ))

(defun ampl-repl ()
  "Runs AMPL in a screen session in a `term' buffer."
  (interactive)
  (require 'term)
  (require 'shell)
  ;; (let ((termbuf (apply 'make-term "AMPL REPL" "screen" nil (split-string-and-unquote "julia"))))
  (let ((termbuf (make-term "AMPL REPL" "ampl")))
    (set-buffer termbuf)
    ;; (term-mode)
    ;; (term-char-mode)
    (switch-to-buffer termbuf)
    (shell-mode)
    (set-process-filter  (get-buffer-process (current-buffer)) 'comint-output-filter )
    ;; (local-set-key (kbd "C-j") 'term-switch-to-shell-mode)
    (compilation-shell-minor-mode 1)
    (comint-send-input)
    ))

(defun ampl-send-file ()
  "Send the current file to the AMPL repl.
It automatically resets the repl before reading the file."
  (interactive)
  (comint-send-string "*AMPL REPL*" (format "reset; include \"%s\";\n" (buffer-file-name))))

(defun ampl-send-region (start end)
  "Send the current selected region to the AMPL repl.
It automatically resets the repl before reading the file."
  (interactive "r")
  (comint-send-region "*AMPL REPL*" start end)
  (comint-send-string "*AMPL REPL*" "\n")
  )

(defun ampl/post-init-company ()
  ;; Need to convince company that this mode is a code mode.
  (with-eval-after-load 'company-dabbrev-code (push 'ampl-mode company-dabbrev-code-modes))
  (spacemacs|add-company-hook ampl-mode))

(defun ampl/post-init-hs-minor-mode ()
  (add-hook 'ampl-mode-hook 'hs-minor-mode))

(defun ampl/post-init-smartparens ()
  (add-hook 'ampl-mode-hook 'smartparens-mode))

;;; packages.el ends here
