;;; packages.el --- pariemacs layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author:  <tref@questor>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `pariemacs-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `pariemacs/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `pariemacs/pre-init-PACKAGE' and/or
;;   `pariemacs/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst pariemacs-packages
  '(
    company
    smartparens
    (pari :location local))
  "The list of Lisp packages required by the pariemacs layer.

Each entry is either:

1. A symbol, which is interpreted as a package to be installed, or

2. A list of the form (PACKAGE KEYS...), where PACKAGE is the
    name of the package to be installed or loaded, and KEYS are
    any number of keyword-value-pairs.

    The following keys are accepted:

    - :excluded (t or nil): Prevent the package from being loaded
      if value is non-nil

    - :location: Specify a custom installation location.
      The following values are legal:

      - The symbol `elpa' (default) means PACKAGE will be
        installed using the Emacs package manager.

      - The symbol `local' directs Spacemacs to load the file at
        `./local/PACKAGE/PACKAGE.el'

      - A list beginning with the symbol `recipe' is a melpa
        recipe.  See: https://github.com/milkypostman/melpa#recipe-format")

(defun pariemacs/init-pari ()
  ;; (require 'pari)
  (use-package pari
    :init
    (autoload 'gp-mode "pari" nil t)
    (autoload 'gp-script-mode "pari" nil t)
    (autoload 'gp "pari" nil t)
    (autoload 'gpman "pari" nil t)
    ;; (define-key evil-insert-state-map (kbd "TAB") 'gp-complete)
    ;; (define-key evil-insert-state-map (kbd "RET") 'electric-indent-just-newline)
    (evil-define-key 'insert gp-script-map
      (kbd "TAB") 'gp-complete
      (kbd "RET") 'electric-indent-just-newline)

    (spacemacs/set-leader-keys-for-major-mode 'gp-script-mode
      ;; Start the GP interpreter
      "ss" 'gp
      "sr" (lambda ()
             "Eval region"
             (interactive)
             (call-interactively 'gp-run-in-region)
             (spacemacs/alternate-window))
      "sR" 'gp-run-in-region
      "sb" 'gp-run-gp
      )

    :mode ("\\.gp\\'" . gp-script-mode)
    )
  )

(defun pariemacs/post-init-company ()
  (spacemacs|add-company-hook gp-script-mode)
  (add-hook 'gp-script-mode-hook 'company-mode))

(defun pariemacs/post-init-smartparens ()
  ;; (add-hook 'pariemacs-mode-hook 'smartparens-mode))
  (add-hook 'gp-script-mode-hook 'smartparens-mode))


;;; packages.el ends here
