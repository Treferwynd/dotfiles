-- This mpv user script will mark currently seen episode on trakt.tv and on a local db
-- You can mark it manually too by pressing 'myshows_mark' (default: W) hotkey.
-- This is ripped off from https://github.com/gim-/mpv-plugin-myshows/blob/master/myshows.lua
--
-- Copyright (c) 2016 Andrejs Mivreņiks
-- Copyright (c) 2016 Treferwynd
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
local mp = require 'mp'
local msg = require 'mp.msg'
local utils = require 'mp.utils'
local options = require 'mp.options'

local http = require 'socket.http'
local json = require 'json'
local ltn12 = require 'ltn12'
local URL = require 'socket.url'

-- To show debug info
-- mpv --msg-level=mark_episode_as_seen=debug

-- CONFIGS
DB_COMMAND_MARK_AS_SEEN = '/home/tref/usr/scripts/check_episodes markEpisodeAsSeen "%s" "%s" "%s"'
EPISODE_NAME_REGEX = '([^%/]+) S(%d+)E(%d+) %- (.+)%.[^.]+$'
MOVIE_NAME_REGEX = '([^%/]+)%.[^.]+$'
MOVIE_NAME_YEAR_REGEX = '(.+) %[(%d+)%]'
CONFIG_FILE_PATH = "/home/tref/.config/check_episodes/trakt_creds.json"
LOG_FILE_PATH = "/home/tref/.config/check_episodes/trakt_log"

local timer_obj = nil   -- Main timer object
local marked = false

-------------------------------------
-- Load configuration from config file
-------------------------------------
function loadHeadersFromConfig()
    local file = io.open(CONFIG_FILE_PATH, "r")
    SETTINGS = json.decode(file:read("*all"))
    file:close()
end

-------------------------------------
-- On file loaded callback
-- @param event Event data table
-- Start the timer only if the file is the Videos/Movies or Videos/TVSeries directories (or subdirs)
-------------------------------------
function on_file_loaded(event)
    abs_path = utils.join_path(mp.get_property("working-directory"), mp.get_property("path"))
    if string.find(abs_path, "Videos/Movies") ~= nil
        or string.find(abs_path, "/tmp/ce_newepisodeslinks") ~= nil
        or string.find(abs_path, "Videos/TVSeries") ~= nil then
        mp.observe_property("pause", "bool", on_pause_change)
        marked = false
        -- message("Beginning")
    end
end

-------------------------------------
-- On file unloaded callback
-- @param event Event data table
-------------------------------------
function on_file_unloaded(event)
    if timer_obj ~= nil then
        -- Stop currently running timer if exists
        timer_obj:stop()
    end
    mp.unobserve_property(on_pause_change)
end

-------------------------------------
-- On playback paused/resumed callback
-- @param property_name Property name (should be always 'pause')
-- @param value New property value (true/false)
-------------------------------------
function on_pause_change(property_name, value)
    if property_name ~= 'pause' or marked then return end
    if value then
        if timer_obj ~= nil then timer_obj:stop() end
        msg.debug('Timer stopped')
        msg.info('Timer stopped')
    else
        setup_timer()
        msg.debug('Timer resumed')
    end
end

-------------------------------------
-- Set up a timer which marks episode as seen in 3/4 of episode duration
-------------------------------------
function setup_timer()
    local time_pos = mp.get_property('time-pos')
    -- mpv returns nil when playback position is at very start
    if time_pos == nil then time_pos = 0 end
    local seconds = mp.get_property('duration')*0.75 - time_pos
    if seconds >= 0 then
        timer_obj = mp.add_timeout(seconds, mark_as_seen)
        msg.debug('Episode will be marked as seen in', seconds, 'seconds')
    end
end

-------------------------------------
-- Message, if verbose is true, also print to screen
-------------------------------------
function message(str, verbose)
    verbose = verbose or false
    msg.debug(str)
    if verbose then
        mp.osd_message(str, 2)
    end
    -- Write to log file
    logFile = io.open(LOG_FILE_PATH, "a")
    logFile:write(string.format("%s: \"%s\": %s\n", os.date("%F %T"), mp.get_property('filename'), str))
    logFile:close()
end

-------------------------------------
-- Query trakt.tv to get the trakt item from the name of the episode/movie
-- itemType is true for type episode, false for type movie
-------------------------------------
function getTraktItem(name, episodeType)
    local query_url = "https://api.trakt.tv/search?query="
    if episodeType then
        query_url = query_url .. URL.escape(name) .. "&type=episode"
    else
        name, year = string.match(name, MOVIE_NAME_YEAR_REGEX)
        query_url = query_url .. URL.escape(name) .. "&type=movie&year=" .. year
    end
    local respbody = {} -- for the response body
    local result, code, respheaders, status = http.request {
        url = query_url,
        headers = {
            ["Content-Type"] = "application/json",
            ["Authorization"] = SETTINGS["Authorization"],
            ["trakt-api-version"] =  2,
            ["trakt-api-key"] =  SETTINGS["trakt-api-key"]
        },
        sink = ltn12.sink.table(respbody)
    }

    if code == 200 then
        return json.decode(respbody[1])[1]
    elseif status == nil then
        error({msg="Couldn't contact Trakt server"})
    elseif code ~= 200 then
        error({msg=status})
    end
end

-------------------------------------
-- POST to trakt.tv to mark `item` as seen
-- itemType is true for type episode, false for type movie
-------------------------------------
function postTraktItem(item, episodeType)
    local payload = {}                         -- Create empty table
    if episodeType then
        payload["episode"] = item["episode"]   -- Add episode's info to the payload
    else
        payload["movie"] = item["movie"]       -- Add movie's info to the payload
    end
                                               -- Maybe use this:
                                               -- http://docs.trakt.apiary.io/#reference/sync/add-to-history/add-items-to-watched-history
    payload["progress"] = 99.9                 -- Add progress so it gets marked as seen
    payload = json.encode(payload)             -- Encode back to string

    local result, code, respheaders, status = http.request {
        method = "POST",
        url = "https://api.trakt.tv/scrobble/stop",
        source = ltn12.source.string(payload),
        headers = {
            ["Content-Type"] = "application/json",
            ["Authorization"] = SETTINGS["Authorization"],
            ["trakt-api-version"] =  2,
            ["trakt-api-key"] =  SETTINGS["trakt-api-key"],
            ["Content-Length"] = string.len(payload)
        },
    }

    if code == 201 or code == 409 then
        return
    elseif status == nil then
        error({msg="Couldn't contact Trakt server"})
    else
        error({msg=status})
    end
end

-------------------------------------
-- Mark currently whatched episode as seen on trakt.tv and on local db
-------------------------------------
function mark_as_seen(verbose)
    verbose = verbose or false
    if timer_obj ~= nil then timer_obj.stop() end

    mp.resume_all() -- Avoid playback lock

    local name, episodeType, success_msg
    local filename = mp.get_property('filename')
    local seriesName, season, episode, episodeName = string.match(filename, EPISODE_NAME_REGEX)
    if seriesName ~= nil and season ~= nil and episode ~= nil then
        -- Episode
        episodeType = true
        local f = io.popen(string.format(DB_COMMAND_MARK_AS_SEEN,
                                         seriesName, season, episode))
        msg.debug("[DB] ", f:read())
        local r = f:close()

        if not r then
            -- message("Could not mark episode as seen in the local database", verbose)
            message("[FAILED] [DB] Could not mark episode as seen in the local database", true)
            return
        end
        name = seriesName .. " " .. episodeName
        success_msg = "Episode marked as seen"
    else
        -- Movie
        episodeType = false
        name = string.match(filename, MOVIE_NAME_REGEX)
        success_msg = "Movie marked as seen"
    end


    -- Get Info on the current movie/episode from Trakt
    local ok, result = pcall(getTraktItem, name, episodeType)
    if not ok then
        message("[FAILED] [GET] " .. result.msg, verbose)
        return
    elseif result == nil then
        if episodeType then
            message("[FAILED] [INFO] Couldn't find info about this episode", verbose)
            return
        else
            message("[FAILED] [INFO] Couldn't find info about this movie", verbose)
            return
        end
    end

    -- If we're here it means we got the info
    if episodeType then
        msg.debug("Trakt Episode: ", string.format("s%de%d %s", result["episode"]["season"], result["episode"]["number"], result["episode"]["title"]))
    else
        msg.debug("Trakt Movie: ", result["movie"]["title"])
    end

    -- POST to Trakt to mark as seen
    local ok, err = pcall(postTraktItem, result, episodeType)

    -- If we're erroring here, it means `GET` was successful, so something serious happened!
    if not ok then
        message("[FAILED] [POST] " .. err.msg, true)
    else
        marked = true
        message(success_msg, verbose)
    end
end

-- Load auth stuff from config file
if not pcall(loadHeadersFromConfig) then
    message("Couldn't load config file: " .. CONFIG_FILE_PATH, true)
    return
end

--TODO: Multiple entries online sometimes

mp.register_event('file-loaded', on_file_loaded)
mp.register_event('end-file', on_file_unloaded)
-- Mark currently whatched episode and show OSD
mp.add_key_binding('W', 'myshows_mark', function() mark_as_seen(true) end)
