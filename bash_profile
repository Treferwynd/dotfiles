#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

HISTFILESIZE=10000

# Needed by lotsa stuff
export XDG_CONFIG_HOME="/home/tref/.config/"
# Needed by dmenu to find .desktop files in ~/usr/applications
export XDG_DATA_DIRS="/usr/local/share/:/usr/share/:/home/tref/usr/"

# Needed by ssh-agent
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# Dotfiles directory
export DOT_DIR="/home/tref/dotfiles/"

# Create temp folders for sources for makepkg
source /etc/makepkg.conf
mkdir -p $SRCDEST
mkdir -p $SRCPKGDEST

## Add stuff to PATH
# Path for local stuff (needed for emacs)
export PATH=$PATH:~/.local/bin

## Dotnet
# Tools (e.g. paket) path
export PATH=$PATH:~/.dotnet/tools/
export DOTNET_ROOT=/opt/dotnet/

## Wine stuff
export WINEARCH="win64"

# Add folders to PATH
PATH=$PATH:~/usr/Vital_Signs/
PATH=$PATH:~/usr/scripts/

PATH=$PATH:~/Dropbox/Prog/PC/

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
