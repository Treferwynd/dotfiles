" All system-wide defaults are set in $VIMRUNTIME/archlinux.vim (usually just
" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" If you prefer the old-style vim functionalty, add 'runtime! vimrc_example.vim'
" Or better yet, read /usr/share/vim/vim74/vimrc_example.vim or the vim manual
" and configure vim to your own liking!

" .vimrc
" See: http://vimdoc.sourceforge.net/htmldoc/options.html for details

" For multi-byte character support (CJK support, for example):
"set fileencodings=ucs-bom,utf-8,cp936,big5,euc-jp,euc-kr,gb18030,latin1

set tabstop=4       " Number of spaces that a <Tab> in the file counts for.

set shiftwidth=4    " Number of spaces to use for each step of (auto)indent.

set showcmd         " Show (partial) command in status line.

set number          " Show line numbers.

set showmatch       " When a bracket is inserted, briefly jump to the matching
                    " one. The jump is only done if the match can be seen on the
                    " screen. The time to show the match can be set with
                    " 'matchtime'.

set hlsearch        " When there is a previous search pattern, highlight all
                    " its matches.

set incsearch       " While typing a search command, show immediately where the
                    " so far typed pattern matches.

set ignorecase      " Ignore case in search patterns.

set smartcase       " Override the 'ignorecase' option if the search pattern
                    " contains upper case characters.

set backspace=2     " Influences the working of <BS>, <Del>, CTRL-W
                    " and CTRL-U in Insert mode. This is a list of items,
                    " separated by commas. Each item allows a way to backspace
                    " over something.

set autoindent      " Copy indent from current line when starting a new line
                    " (typing <CR> in Insert mode or when using the "o" or "O"
                    " command).

set formatoptions=c,q,r,t " This is a sequence of letters which describes how
                    " automatic formatting is to be done.
                    "
                    " letter    meaning when present in 'formatoptions'
                    " ------    ---------------------------------------
                    " c         Auto-wrap comments using textwidth, inserting
                    "           the current comment leader automatically.
                    " q         Allow formatting of comments with "gq".
                    " r         Automatically insert the current comment leader
                    "           after hitting <Enter> in Insert mode.
                    " t         Auto-wrap text using textwidth (does not apply
                    "           to comments)

set ruler           " Show the line and column number of the cursor position,
                    " separated by a comma.

set background=dark " When set to "dark", Vim will try to use colors that look
                    " good on a dark background. When set to "light", Vim will
                    " try to use colors that look good on a light background.
                    " Any other value is illegal.

set mouse=a         " Enable the use of the mouse.

set clipboard=unnamedplus,exclude:cons\|linux     " Use system clipboard

set noexpandtab     " Use tabs instead of spaces

set modeline        " Use modelines

set nowildmenu      " NOT Show line with all possible completions for : commands

set wildmode=list:longest,full   " Cycle through : commands completions

set scrolloff=3 " Keep 3 lines below and above the cursor while scrolling

" set backup directory
set backupdir=$HOME/.vim/backupfiles,/tmp
set directory=$HOME/.vim/swapfiles//,/tmp


filetype plugin indent on
syntax on

"""" PLUGINS
" Manage plugins with vim-plug
call plug#begin()
	" For everybodyz
	" TODO: for gitgutter check if lazy load actually works
	Plug 'junegunn/vim-easy-align'           " Align dem stuff
	Plug 'matze/vim-move'                    " Move Lines
	Plug 'nanotech/jellybeans.vim'           " Jellybeans theme
	Plug 'terryma/vim-multiple-cursors'      " Multiple cursors
	Plug 'tpope/vim-commentary'              " Un/comment with C-/
	Plug 'triglav/vim-visual-increment'      " C-a and C-x to increase/decrease number and letters
	Plug 'vim-scripts/taglist.vim'           " Taglist source code browser

	let g:installed_gitgutter = 1            " Git stuff in vim
	Plug 'airblade/vim-gitgutter',           { 'on': [ 'GitGutterToggle', 'GitGutterLineHighlightsToggle' ] }

	"" Questor Specific
	if hostname() == "questor"
		Plug 'bling/vim-airline'             " Cool status line
		let g:installed_incsearch = 1        " Highlights all search results while typing
		Plug 'haya14busa/incsearch.vim'
		Plug 'kshenoy/vim-signature'         " Cool markers
		Plug 'ntpeters/vim-airline-colornum' " Highlight current line number

		Plug 'ryanoasis/vim-devicons'        " Cool icons, should be loaded as late as possible

		" On demand
		                                     " Taskwarrior directly inside vim
		Plug 'farseer90718/vim-taskwarrior', { 'on': 'TW' }

		" Syntax
		Plug 'PotatoesMaster/i3-vim-syntax', { 'for': 'i3' }
	endif

	" On demand
	                                         " Show file browser
	Plug 'scrooloose/nerdtree',              { 'on': 'NERDTreeToggle' }

call plug#end()

"" Airline
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#hunks#enabled = 1

let g:airline_colornum_enabled = 1

"" airline-colornum
set cursorline
hi clear CursorLine

"" easy-align
" Start interactive EasyAlign in visual mode (e.g. vip<Enter>)
vmap <Enter> <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

"" Task
" Default view
let g:task_report_command  = ['simple']
let g:task_report_name = 'simple'
" default fields to ask when adding a new task
let g:task_default_prompt  = ['description', 'project']

"" Ultisnips
let g:UltiSnipsExpandTrigger = "<nop>"
let g:ulti_expand_or_jump_res = 0
function ExpandSnippetOrCarriageReturn()
    let snippet = UltiSnips#ExpandSnippetOrJump()
    if g:ulti_expand_or_jump_res > 0
        return snippet
    else
        return "\<CR>"
    endif
endfunction

inoremap <expr> <CR> pumvisible() ? "<C-R>=ExpandSnippetOrCarriageReturn()<CR>" : "\<CR>"
""" TODO: this is probably a better alternative:
"" http://stackoverflow.com/a/18685821
" To enalbe ctrl-k shortcut
inoremap <c-x><c-k> <c-x><c-k>
" Cycle trough placeholders
let g:UltiSnipsJumpForwardTrigger="<cr>"
let g:UltiSnipsJumpBackwardTrigger="<c-l>"

"" Taglist plugin
let Tlist_Compact_Format = 1
let Tlist_GainFocus_On_ToggleOpen = 1
"let Tlist_Close_On_Select = 1

"" Commentary to ctrl-7 and ctrl-/
imap <C-_> <Esc>gcc
nmap <C-_> gcc
vmap <C-_> gc

"" Incremental Search "
" Check if incsearch is installed (or just loaded)
if exists('g:installed_incsearch')
	map /  <Plug>(incsearch-forward)
	map ?  <Plug>(incsearch-backward)
	map g/ <Plug>(incsearch-stay)

	" Automatic no highlight
	set hlsearch
	let g:incsearch#auto_nohlsearch = 1
	map n  <Plug>(incsearch-nohl-n)
	map N  <Plug>(incsearch-nohl-N)
	map *  <Plug>(incsearch-nohl-*)
	map #  <Plug>(incsearch-nohl-#)
	map g* <Plug>(incsearch-nohl-g*)
	map g# <Plug>(incsearch-nohl-g#)
endif

"" GitGutter
" Check if GitGutter is installed (or just loaded)
let g:gitgutter_enabled = 0
let g:gitgutter_signs = 1
let g:gitgutter_highlight_lines = 0

if exists('g:installed_gitgutter')
	" Use n/N to navigate through the changed hunks
	function! ToggleGitGutting()
		GitGutterLineHighlightsToggle
		if g:gitgutter_highlight_lines
			nmap n <Plug>GitGutterNextHunk
			nmap N <Plug>GitGutterPrevHunk
		else
			unmap n
			unmap N
		endif
	endfunction

	inoremap <F5> <Esc>:GitGutterToggle<CR>a
	nnoremap <F5> :GitGutterToggle<CR>
	inoremap <F6> <Esc>:call ToggleGitGutting()<CR>a
	nnoremap <F6> :call ToggleGitGutting()<CR>
endif

"" vim-visual-increment
" Increment also letters, octal and hex!
set nrformats=alpha,octal,hex

"" vim-move
" Move lines and blocks of text!
let g:move_map_keys = 0
vmap <C-k> <Plug>MoveBlockDown
vmap <C-l> <Plug>MoveBlockUp
nmap <C-k> <Plug>MoveLineDown
nmap <C-l> <Plug>MoveLineUp

""""""""""""
" MAPPINGS "
""""""""""""

" Lots of people remapped <Space> to <Leader>
noremap <Space> :

" Paste on new line with è:
noremap è :pu<CR>

" Move current line down
function MoveLineDown()
	:s/^/\r/g
	:noh
endfunction
noremap + :call MoveLineDown()<CR>

"" Spell checker
" Use n/N to navigate through the changed hunks
function! ToggleSpellCheck()
	let &spell = !&spell
	if &spell
		map n ]s
		map N [s
	else
		unmap n
		unmap N
	endif
endfunction
noremap <F10> :call ToggleSpellCheck()<CR>

inoremap <C-s-l> <Esc>:TlistToggle<CR>
nnoremap <C-s-l> :TlistToggle<CR>
" Control-s to save
inoremap <C-s> <Esc>:w<CR>
nnoremap <C-s> :w<CR>
" Control-w to save
"inoremap <C-w> <Esc>:w<CR>
"nnoremap <C-w> :w<CR>
" Control-q to quit
inoremap <C-q> <Esc>:q<CR>
nnoremap <C-q> :q<CR>
" Save and run
inoremap <F9> <Esc>:w<CR>:!%:p<CR>
nnoremap <F9> :w<CR>:!%:p<CR>
" Change window
nnoremap <C-H> <C-W>h
nnoremap <C-J> <C-W>l
" Toogle NERDTree
inoremap <C-o> <Esc>:NERDTreeToggle<CR>
nnoremap <C-o> :NERDTreeToggle<CR>
" Control-q to quit
inoremap <C-Q> <Esc>:q!<CR>
nnoremap <C-Q> :q!<CR>

"" Keys
" Remap arrows jkl; (also move inside wrapped lines)
noremap j h
noremap k gj
noremap l gk
noremap ; l
noremap ò l

" jk to esc
imap jk <Esc>

" Tab to manage indentation
nnoremap <Tab> >>
nnoremap <S-Tab> <<
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv
" TODO: IT SHOULD WORK
inoremap <expr> <S-Tab> pumvisible() ? "<C-p>" : "<C-d>"

" Y to yank to end of line
map Y y$

"""""""""""""""""
" Custom Colors "
"""""""""""""""""
" 80 char limit
highlight OverLength ctermbg=235
match OverLength /\%79v.\+/

" TODO labels
if !exists("autocmd_colorscheme_loaded")
  let autocmd_colorscheme_loaded = 1
  autocmd ColorScheme * highlight Todo       ctermbg=64    ctermfg=225      cterm=bold
  autocmd ColorScheme * highlight TodoMild   ctermbg=208   ctermfg=225      cterm=bold
  autocmd ColorScheme * highlight TodoCrit   ctermbg=88    ctermfg=White    cterm=bold
  autocmd ColorScheme * highlight Info       ctermbg=27    ctermfg=White    cterm=bold
endif

if has("autocmd")
	" Highlight TODO, TODO2, TODO3, NOTE, etc.
	if v:version > 701
		autocmd Syntax * call matchadd('Todo',     '\W\zs\(TODO1:\|TODO:\|WIP:\|TEST:\)')
		autocmd Syntax * call matchadd('Todo',     '\W\zs\(TODO1\|TODO\|WIP\|TEST\)')
		autocmd Syntax * call matchadd('TodoMild', '\W\zs\(TODO2:\|DEBUG:\|HACK:\)')
		autocmd Syntax * call matchadd('TodoMild', '\W\zs\(TODO2\|DEBUG\|HACK\)')
		autocmd Syntax * call matchadd('TodoCrit', '\W\zs\(TODO3:\|BUG:\|FIXME:\)')
		autocmd Syntax * call matchadd('TodoCrit', '\W\zs\(TODO3\|BUG\|FIXME\)')
		autocmd Syntax * call matchadd('Info',     '\W\zs\(NOTE:\|AXIOMS:\|AXIOM:\|IDEA:\|INFO:\|N\.B\.:\|NB:\)')
		autocmd Syntax * call matchadd('Info',     '\W\zs\(NOTE\|AXIOMS\|AXIOM\|IDEA\|INFO\|N\.B\.\|NB\)')
	endif
endif

"""""""""
" Theme "
"""""""""
"let g:molokai_original = 1
colo jellybeans

" Visualize indentation tabs
set list
set listchars=tab:\│\ ,extends:>,precedes:<
"set listchars=eol:¬,
hi SpecialKey ctermbg=233 ctermfg=234

" Navigate more easily in vimdiff
if &diff
	noremap <space> ]cz.
	noremap n ]cz.
	noremap N [cz.
endif

"""""""""""""
" File type "
"""""""""""""
autocmd FileType python setlocal tabstop=4 shiftwidth=4 noexpandtab
autocmd BufReadPost PKGBUILD  set filetype=PKGBUILD

