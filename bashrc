#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Set vi mode (activate vi mode by pressing Esc)
set -o vi

# Auto "cd" when entering just a path
shopt -s autocd

# Vim control-s stuff
stty -ixon

# don't put duplicate lines in the history. See bash(1) for more options
export HISTCONTROL=ignoreboth

# Editor
export EDITOR=nvim

# Browser
if [ -n "$DISPLAY" ]; then
	export BROWSER=firefox
else
	export BROWSER=links
fi

# Adjust LS Colors
LS_COLORS="ow=01;34"
export LS_COLORS

# Enable colors and case insensitive search in LESS and other stuff
export LESS="-RSMsi"

## Bash completion
# User defined bash completion
if [ -n "$DOT_DIR" ]; then
	for completion_file in "$DOT_DIR"/bash_completion.d/*; do
		source "$completion_file";
	done
fi

# "command not found" hook
if [ -f /usr/share/doc/pkgfile/command-not-found.bash ]; then
	source /usr/share/doc/pkgfile/command-not-found.bash
fi

# z to jump around directories
if [ -f /usr/lib/z.sh ]; then
	source /usr/lib/z.sh
fi

# partial search of bash history by typing the first letters before using the up/down keys
if [[ $- == *i* ]]
then
    bind '"\e[A": history-search-backward'
    bind '"\e[B": history-search-forward'
fi


## Wine shit:
# https://wiki.archlinux.org/index.php/Wine#Prevent_new_Wine_file_associations
export WINEDLLOVERRIDES="winemenubuilder.exe=d"

#                   #
#       Alias       #
#                   #

alias s='sudo'

# color stuff
alias less='less -r'
alias rg='rg --color=always'

alias ls='ls -FNh --tabsize=0 --color=always --show-control-chars --group-directories-first'
alias l='ls'
alias la='ls -A'
alias lh='ls -d .*'
alias ll='ls -l'
alias lsf='ls -Ssr1'
alias lss='ls | less'
alias lsize='/usr/bin/du -sch * 2>/dev/null | /usr/bin/sort -h'
alias lsizec='/usr/bin/du -sb .?[!.]* * 2>/dev/null | sort -n | /usr/bin/python3 ~/usr/Vital_Signs/cumulative.py'
alias lsizeh='/usr/bin/du -shc .?[!.]* * 2>/dev/null | sort -h | /usr/bin/sort -h'
alias lsizel='/usr/bin/du -schL * 2>/dev/null | /usr/bin/sort -h'

alias cdot='cd ~/dotfiles'
alias cl='clear'
alias dosbox='dosbox /home/tref/Dropbox/Games/'
alias dosboxfg='dosbox /home/tref/Dropbox/Games/FGodmom/ab150'
alias dualvlc='/home/$USER/usr/scripts/dualvlc.sh'
alias e='emacsclient -nc'
alias fixscreen='/home/$USER/usr/scripts/dualscreen.sh'
alias fixxflux='/usr/bin/xflux -l 45,58 -g 8,90 -k 3200'
alias grep='grep --color=auto --binary-file=without-match'
alias hi='history'
alias myip='curl icanhazip.com'
alias nano='nano -w'
alias nbash='nano ~/dotfiles/bashrc && update'
alias ni3='nano ~/dotfiles/config/i3/config'
alias op='xdg-open'
alias pgg='ping 8.8.8.8'
alias pulserestart='pulseaudio -k; pulseaudio'
alias py='python3'
alias py2='python2'
alias py3='python3'
alias reboot='sudo reboot'
alias root='sudo su'
alias showkeys='xmodmap -pke'
alias shutdown='sudo shutdown -h now'
alias snano='sudo nano'
alias steam-wine='WINEDEBUG=-all optirun wine ~/.wine/drive_c/Program\ Files\ \(x86\)/Steam/Steam.exe -no-dwrite'
alias tmpgit='cd ~/tmpgit && git clone '
alias wakeheadphones='aplay /usr/share/sounds/alsa/Front_Center.wav'
alias weechat='weechat --dir /home/tref/.config/weechat/'
alias where='type -p'
alias words='cat /usr/share/dict/words'
alias xev='xev | grep -A2 --line-buffered "^KeyRelease" | sed -n "/keycode /s/^.*keycode \([0-9]*\).* (.*, \(.*\)).*$/\1 \2/p"'

alias skyrim='optirun -b primus wine /home/tref/.wine/dosdevices/c\:/Program\ Files\ \(x86\)/TSEV\ Skyrim\ LE/SkyrimLauncher.exe'
alias killskyrim='pkill "TESV\.exe"'

alias h='history |grep '
alias f='find . |grep '
alias fii='find . |grep -i'
alias fd='fd --follow --no-ignore'
alias fdi='fd'
alias psa='ps aux |grep -i '
alias c='cat ~/Dropbox/Codici/coduti | grep'
alias cs='cat ~/Dropbox/Codici/coduti'

alias hi='history| grep -i'
alias fn='find . | grep -i'


alias psai='ps aux | grep -i'
alias ci='cat ~/Dropbox/Codici/coduti | grep -i'

alias nexus-connect='go-mtpfs /media/Nexus'
alias nexus-disconnect='fusermount -u /media/Nexus'
# get full path from file
alias fp='readlink -f'

## Clipboard and Snippets Utils
alias xo='xclip -selection clipboard -o '
alias cfile='xo > '
alias csnippet='xo | curl -X POST https://api.bitbucket.org/2.0/snippets/Treferwynd -u Treferwynd -F file=@-'
alias csnippetscc='xo | curl -X POST https://api.bitbucket.org/2.0/snippets/SiriusCC -u Treferwynd -F file=@-'
alias ixio='curl -F "f:1=<-" ix.io'
alias cixio='xo | ixio'
alias cpb='xo | curl -F c=@- https://ptpb.pw'

## Shortcuts
alias ce='check_episodes showNewEpisodes'
alias cetw='check_episodes getEpisodesToWatch'
alias d='rg --max-columns 300'
alias di='rg --max-columns 300 -i'
alias g='grep'
alias gi='grep -i'

## Navigation
alias ù='cd'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias cd..='cd ..'
alias ù='cd'
alias back='cd $OLDPWD'
alias c-='cd -'

## Git
alias ga='git add'
alias gA='git add -A'
alias gap='git add -p'
alias gca='git commit -a'
alias gcm='git commit -m'
alias gcam='git commit -am'
alias gd='git diff'
alias gl='git log --stat'
alias gp='git push'
alias gs='git status'
alias gsh='git show'
alias gull='git pull'

## Pacman
alias Pacman='sudo pacman'
alias pacleanu='paccache -ruv -c /var/cache/pacman/pkg -k 0 && paccache -ruv -c /var/cache/usrpkg -k 0'
alias pacleank='paccache -rv -c /var/cache/pacman/pkg -k 0 && paccache -rv -c /var/cache/usrpkg -k 0'
# alias yay='yay --color=always'
alias pas='yay -Ss'
alias pasa='yay -Ssa'
alias pasn='yay -Ss --by name'
alias pasan='yay -Ssa --by name'
alias paso='pacman -Ss'
alias p='pacman -Q | grep '
alias pi='pacman -Q | grep -i'
alias piq='pacman -Qq | grep -i'
alias pql='pacman -Qlq | grep -i'
alias prcsn='sudo pacman -Rcsn'
alias pin='sudo pacman -S'
alias psyu='sudo pacman -Syu'
alias psyyu='sudo pacman -Syyu'
alias pau='echo "yay -u --noedit" && yay -u --noedit'
# Use pacmatic instead of pacman if it is available
if [ -n "pacman -Qs pacmatic" ]; then
	alias pin='sudo pacmatic -S'
	alias psyu='sudo pacmatic -Syu'
	alias psyyu='sudo pacmatic -Syyu'
fi

# '[r]emove [o]rphans' - recursively remove ALL orphaned packages
alias pacro="/usr/bin/pacman -Qtdq > /dev/null && sudo /usr/bin/pacman -Rns \$(/usr/bin/pacman -Qtdq | sed -e ':a;N;\$!ba;s/\n/ /g')"

# Use UTF-8 in screen
alias screen='screen -U'

## SSH
alias ssh='TERM=xterm-256color ssh'
alias sshdstart='sudo systemctl start sshd'
alias sshdstop='sudo systemctl stop sshd'
alias sshmediateca='ssh -p 27182 mediateca@192.168.2.9'
alias sshmediatecascr='ssh -t -p 27182 mediateca@192.168.2.9 "screen -dUR autoscreen"'
alias sshquestor='ssh -X -p 27182 tref@192.168.2.3'

alias sshpi='ssh tref@192.168.1.9'
alias sshpiscr='ssh -t tref@192.168.1.9 "screen -dUR autoscreen"'
alias sshpiroot='ssh root@192.168.1.9'

alias sshpimanverie='ssh osmc@192.168.1.104'
alias sshpiscrmanverie='ssh -t manverie@192.168.1.104 "export LANG=en_US.UTF-8; screen -dUR autoscreen"'
alias sshpimcasa='ssh casa@192.168.0.19'
alias sshpiscrmcasa='ssh -t casa@192.168.0.19 "export LANG=en_US.UTF-8; screen -dUR autoscreen"'
alias sshpimroot='ssh root@192.168.0.17'

alias sshbigmanverie='ssh manverie@192.168.0.111'
alias sshblog='ssh fromital@fromitalywithfood.com -p18765'

## Task
alias ts='task simple'
alias tsp='task_show_project'
alias vit='vim -c TW'

############# WIP TEST #############
alias vim='nvim'
## Vim
alias v='vim'
alias vbash='vim ~/dotfiles/bashrc && update'
alias vi3='vim ~/dotfiles/config/i3/config'
alias vimcleancache='rm ~/.vim/swapfiles/*'
alias vimrc='vim ~/dotfiles/vimrc'
alias nvimrc='vim ~/dotfiles/nvimrc'
alias vtermite='vim ~/dotfiles/config/termite/config'
alias svim='sudo vim'
alias svimdiff='sudo vimdiff'

#                                    #
#               Prompt               #
#                                    #

# Default
#PS1='[\u@\h \w]\$ '

# Username
user='\[\033[38;5;34m\]'
# @
at='\[\033[38;5;22m\]'
# Hostname
host='\[\033[38;5;28m\]'
# Path
path='\[\033[38;5;27m\]'
# $
sep='\[\033[38;5;93m\]'
# No color
nc='\[\033[00m\]'

PS1="${user}\u${at}@${host}\h ${path}\w ${sep}$ ${nc}"

### SSH PS
# Hostname
hostSSH='\[\033[38;5;46m\]'
pathSSH='\[\033[38;5;27m\]'

if [ -n "$SSH_CLIENT" ]; then
	PS1="${user}\u${at}@${hostSSH}***\h*** ${pathSSH}\w ${sep}$ ${nc}"
fi

### Screen PS
hostSCREEN='\[\033[38;5;48m\]'
pathSCREEN='\[\033[38;5;27m\]'
if [[ -n $STY ]]; then
	PS1="${user}\u${at}@${hostSCREEN}***\h*** ${pathSCREEN}\w ${sep}$ ${nc}"
fi

##### FUNCTIONS #####
# Convert to human readable numbers
hr () {
    echo $1 | awk '{xin=$1;if(xin==0){print "0 B";}else{x=(xin<0?-xin:xin);s=(xin<0?-1:1);split("B KiB MiB GiB TiB PiB",type);for(i=5;y < 1;i--){y=x/(2^(10*i));}print y*s " " type[i+2];};}';
}

rungo () {
    go build "${1%%.go}.go"
    $(fp "${1%%.go}")
}

# Measure time between tic and toc
tic () {
    date +"%s%3N" > /tmp/tic
}
toc () {
    echo $(($(date +"%s%3N")-$(cat /tmp/tic))) ms
}

# Run a function multiple times
run() {
    number=$1
    shift
    for i in `seq $number`; do
        $@
    done
}
# Run a function multiple times and output the average time
runtime() {
    quiet=""
    if [ $1 = "-q" ]; then
        quiet="quiet"
        shift
    fi
    number=$1
    shift
    tot=0
    max=-1
    min=10000
    for i in `seq $number`; do
        tmp=$({ time $@ > /dev/null; }  2>&1 | perl -ne 'print if s/(real\s+0m)([\.\d]*)(s)/\2/;')
        tot=$(python -c "print($tot + $tmp)")
        max=$(python -c "print(max($max, $tmp))")
        min=$(python -c "print(min($min, $tmp))")
        if [ -z "$quiet" ]; then
            python -c "print('{:4} / {:<4}: {} s'.format($i, $number, $tmp))";
        fi
    done
    python -c "print('Average:    {:7.3f} s'.format($tot/$number))"
    python -c "print('Max:        {:7.3f} s'.format($max))"
    python -c "print('Min:        {:7.3f} s'.format($min))"
    python -c "print('Total:      {:7.3f} s'.format($tot))"
}
# Stop the i3autokeyboardlayout script, run the specified command, restart i3autokeyboardlayout
gamify () {
    # TODO: super not safe
    /usr/bin/killall --quiet perl
    /usr/bin/killall --quiet xcape
    # Load the us keyboard without options
    setxkbmap us -option
    $@
    echo wow
    /usr/bin/killall --quiet perl; /home/$USER/usr/scripts/i3autokeyboardlayout
}

# EXTRACT FUNCTION
#######################################################

extract () {
	if [ -f $1 ] ; then
		case $1 in
			*.tar.gz)    tar xzf $1     ;;
			*.tar.bz2)   tar xjf $1        ;;
			*.bz2)       bunzip2 $1       ;;
			*.rar)       unrar x $1     ;;
			*.gz)        gunzip $1     ;;
			*.tar)       tar xf $1        ;;
			*.tbz2)      tar xjf $1      ;;
			*.tgz)       tar xzf $1       ;;
			*.zip)       unzip $1     ;;
			*.Z)         uncompress $1  ;;
			*.7z)        7z x $1    ;;
			*)           echo "'$1' cannot be extracted via extract()" ;;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}

# Get pkg from AUR
aget () {
	if [ -n "$1" ]; then
		# If directory already exist
		if [ -d "~/aur/$1" ]; then
			echo "Overriding existing directory"
			rm "~/aur/$1" -r
		fi
		cd ~/aur
        git clone https://aur.archlinux.org/$1.git
		cd $1
		echo "The package $1 contains:"
		ls
	else
		echo "One argument is required"
	fi
}


#copy and go to dir
cpg () {
	if [ -d "$2" ];then
		cp $1 $2 && cd $2
	else
		cp $1 $2
	fi
}

#move and go to dir
mvg (){
	if [ -d "$2" ];then
		mv $1 $2 && cd $2
	else
		mv $1 $2
	fi
}

# source .bashrc
update (){
	source ~/.bashrc
}

# Fix dropbox's conflicted files
fixconflicted (){
	find Dropbox/ | grep onflicted | sed 's/^\.\///g' |
		while read i
		do
			echo "$i";
			rm "$i";
		done
}

# Task show project
task_show_project() {
	ts project:"$1"
}

# Speak to me
say() { curl -s -A "Mozilla/5.0" "http://translate.google.com/translate_tts?tl=en&q=${@// /+}" | play -t mp3 -; }
sayit() { curl -s -A "Mozilla/5.0" "http://translate.google.com/translate_tts?tl=it&q=${@// /+}" | play -t mp3 -; }

# Color ze man pages
man() {
	env LESS_TERMCAP_mb=$'\E[01;31m' \
	LESS_TERMCAP_md=$'\E[01;38;5;74m' \
	LESS_TERMCAP_me=$'\E[0m' \
	LESS_TERMCAP_se=$'\E[0m' \
	LESS_TERMCAP_so=$(tput setaf 166) \
	LESS_TERMCAP_ue=$'\E[0m' \
	LESS_TERMCAP_us=$'\E[04;38;5;146m' \
	man "$@"
}

# Cat from alias
# Does not work for shell functions (e.g. wot wat)
wot ()
{
	cat `type -p "$1"`
}
# Edit stuff from alias
vimwhere ()
{
	vim `where $1`
}
# Dafuq is this
# does not work with irssi
wat()
{
	RED='\e[0;31m'
	BLUE='\e[0;34m'
	NC='\e[0m' # No Color
	desc=$(pacman -Qi "$1" 2>&1 | grep Description | sed 's/Description\s*:\s*//g')
	if [[ -n $desc	]]; then
		echo -e "$RED$1$NC\t:\t$desc"
	else
		pack=$(pkgfile "$1" | head -n 1 | grep -Eo "[^\/]*$")
		if [[ -n $pack ]]; then
			echo -e "$BLUE$1$NC is in\t:\t$RED$pack$NC"
			wat $pack
		else
			echo "I'll be damned if I know"
		fi
	fi
}

## fzf stuff

export FZF_DEFAULT_COMMAND='rg --files --follow'
export FZF_DEFAULT_OPTS='--bind J:down,K:up'
[ -f /usr/share/fzf/completion.bash ] && source /usr/share/fzf/completion.bash
[ -f /usr/share/fzf/key-bindings.bash ] && source /usr/share/fzf/key-bindings.bash

# Go to directory with C-o
builtin bind -x '"\C-x1": fz'
builtin bind '"\C-o": "\C-x1\n"'

# fz - cd to selected directory
fz() {
  local dir
  dir=$(find ${1:-.} -path '*/\.*' -prune \
                  -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir"
}

# Find file with C-f
builtin bind -x '"\C-x2": __fzf_select_dir'
builtin bind '"\C-f": "\C-x2\e^\er"'

# Another CTRL-T script to select a directory and paste it into line
__fzf_select_dir ()
{
    builtin typeset READLINE_LINE_NEW="$(
        command find -L . \( -path '*/\.*' -o -fstype dev -o -fstype proc \) \
            -prune \
            -o -type f -print \
            -o -type d -print \
            -o -type l -print 2>/dev/null \
        | command sed 1d \
        | command cut -b3- \
        | env fzf -m
    )"

    if
        [[ -n $READLINE_LINE_NEW ]]
    then
        builtin bind '"\er": redraw-current-line'
        builtin bind '"\e^": magic-space'
        READLINE_LINE=${READLINE_LINE:+${READLINE_LINE:0:READLINE_POINT}}${READLINE_LINE_NEW}${READLINE_LINE:+${READLINE_LINE:READLINE_POINT}}
        READLINE_POINT=$(( READLINE_POINT + ${#READLINE_LINE_NEW} ))
    else
        builtin bind '"\er":'
        builtin bind '"\e^":'
    fi
}

# fza - including hidden directories
fza() {
    local dir
    dir=$(find ${1:-.} -type d 2> /dev/null | fzf +m) && cd "$dir"
}

# fkill - kill process
fkill() {
    pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')

    if [ "x$pid" != "x" ]
    then
        kill -${1:-9} $pid
    fi
}

# fe [FUZZY PATTERN] - Open the selected file with the default editor
#   - Bypass fuzzy finder if there's only one match (--select-1)
#   - Exit if there's no match (--exit-0)
fe() {
  local files
  IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
  [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
}

# Termite Stuff for opening new terminal
if [[ $TERM == xterm-termite ]]; then
	. /etc/profile.d/vte.sh
	__vte_prompt_command
fi
