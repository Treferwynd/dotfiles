;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '("~/dotfiles/spacemacs_layers/")
   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(typescript
     ;; ampl
     (auto-completion :variables
                      ;; auto-completion-return-key-behavior 'complete
                      auto-completion-tab-key-behavior 'cycle
                      ;; auto-completion-complete-with-key-sequence-delay 0
                      auto-completion-idle-delay 0.2
                      company-quickhelp-delay 0
                      ;; Show docstring tooltips
                      auto-completion-enable-help-tooltip t
                      ;; Sort results by usage
                      auto-completion-enable-sort-by-usage t
                      ;; Show snippets in auto-completion popup
                      auto-completion-enable-snippets-in-popup t
                      ;; Enable all snippets
                      ;; auto-completion-private-snippets-directory t
                      )
     c-c++
     clojure
     csharp
     csv
     emacs-lisp
     fsharp
     git
     gtags
     ;; (haskell :variables haskell-completion-backend 'intero)
     ;; (haskell :variables haskell-completion-backend 'ghc-mod)
     ;; HIE stuff
     (haskell :variables ;; Or optionally just haskell without the variables.
              ;; haskell-completion-backend 'ghci
              ;; haskell-process-type 'stack-ghci
              haskell-enable-hindent t
              )
     html
     ivy
     javascript
     latex
     lsp
     lua
     markdown
     multiple-cursors
     org
     ;; pariemacs
     (python :variables
             python-backend 'lsp
             python-lsp-server 'mspyls)
     ;; pdf-tools
     spell-checking
     syntax-checking
     treemacs
     (version-control :variables
                      version-control-diff-side 'left
                      ;; version-control-diff-tool 'diff-hl
                      version-control-diff-tool 'git-gutter+
                      version-control-global-margin t)
     ;; better-defaults
     (shell :variables
            shell-default-height 30
            shell-default-position 'bottom)

     yaml
     )

   ;; List of additional packages that will be installed without being wrapped
   ;; in a layer (generally the packages are installed only and should still be
   ;; loaded using load/require/use-package in the user-config section below in
   ;; this file). If you need some configuration for these packages, then
   ;; consider creating a layer. You can also put the configuration in
   ;; `dotspacemacs/user-config'. To use a local version of a package, use the
   ;; `:location' property: '(your-package :location "~/path/to/your-package/")
   ;; Also include the dependencies as they will not be resolved automatically.
   dotspacemacs-additional-packages
   '(
     boogie-friends
     browse-at-remote
     dimmer
     futhark-mode
     fzf
     ;; HIE stuff
     (lsp-haskell :location (recipe :fetcher github :repo "emacs-lsp/lsp-haskell"))
     ;; yasnippet
     )
   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '()

   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need to
   ;; compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;;
   ;; WARNING: pdumper does not work with Native Compilation, so it's disabled
   ;; regardless of the following setting when native compilation is in effect.
   ;;
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; Name of executable file pointing to emacs 27+. This executable must be
   ;; in your PATH.
   ;; (default "emacs")
   dotspacemacs-emacs-pdumper-executable-file "emacs"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=$HOME/.emacs.d/.cache/dumps/spacemacs-27.1.pdmp
   ;; (default (format "spacemacs-%s.pdmp" emacs-version))
   dotspacemacs-emacs-dumper-dump-file (format "spacemacs-%s.pdmp" emacs-version)

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; Set `read-process-output-max' when startup finishes.
   ;; This defines how much data is read from a foreign process.
   ;; Setting this >= 1 MB should increase performance for lsp servers
   ;; in emacs 27.
   ;; (default (* 1024 1024))
   dotspacemacs-read-process-output-max (* 1024 1024)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. Spacelpa is currently in
   ;; experimental state please use only for testing purposes.
   ;; (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default t)
   dotspacemacs-verify-spacelpa-archives t

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'vim

   ;; If non-nil show the version string in the Spacemacs buffer. It will
   ;; appear as (spacemacs version)@(emacs version)
   ;; (default t)
   dotspacemacs-startup-buffer-show-version t

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official

   ;; Scale factor controls the scaling (size) of the startup banner. Default
   ;; value is `auto' for scaling the logo automatically to fit all buffer
   ;; contents, to a maximum of the full image height and a minimum of 3 line
   ;; heights. If set to a number (int or float) it is used as a constant
   ;; scaling factor for the default logo size.
   dotspacemacs-startup-banner-scale 'auto

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `recents-by-project' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   ;; The exceptional case is `recents-by-project', where list-type must be a
   ;; pair of numbers, e.g. `(recents-by-project . (7 .  5))', where the first
   ;; number is the project limit and the second the limit on the recent files
   ;; within a project.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Show numbers before the startup list lines. (default t)
   dotspacemacs-show-startup-list-numbers t

   ;; The minimum delay in seconds between number key presses. (default 0.4)
   dotspacemacs-startup-buffer-multi-digit-delay 0.4

   ;; If non-nil, show file icons for entries and headings on Spacemacs home buffer.
   ;; This has no effect in terminal or if "all-the-icons" package or the font
   ;; is not installed. (default nil)
   dotspacemacs-startup-buffer-show-icons nil

   ;; Default major mode for a new empty buffer. Possible values are mode
   ;; names such as `text-mode'; and `nil' to use Fundamental mode.
   ;; (default `text-mode')
   dotspacemacs-new-empty-buffer-major-mode 'text-mode

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode

   ;; If non-nil, *scratch* buffer will be persistent. Things you write down in
   ;; *scratch* buffer will be saved and restored automatically.
   dotspacemacs-scratch-buffer-persistent nil

   ;; If non-nil, `kill-buffer' on *scratch* buffer
   ;; will bury it instead of killing.
   dotspacemacs-scratch-buffer-unkillable nil

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-dark
                         spacemacs-light)

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spaceline theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(spacemacs :separator wave :separator-scale 1.5)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font or prioritized list of fonts. The `:size' can be specified as
   ;; a non-negative integer (pixel size), or a floating-point (point size).
   ;; Point size is recommended, because it's device independent. (default 10.0)
   dotspacemacs-default-font '("DejaVu Sans Mono"
                               :size 15
                               :weight normal
                               :width normal)

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m" for terminal mode, "<M-return>" for GUI mode).
   ;; Thus M-RET should work as leader key in both GUI and terminal modes.
   ;; C-M-m also should work in terminal mode, but not in GUI mode.
   dotspacemacs-major-mode-emacs-leader-key (if window-system "<M-return>" "C-M-m")

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names t

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, after you
   ;; paste something, pressing `C-j' and `C-k' several times cycles through the
   ;; elements in the `kill-ring'. (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil

   ;; If non-nil the frame is undecorated when Emacs starts up. Combine this
   ;; variable with `dotspacemacs-maximized-at-startup' in OSX to obtain
   ;; borderless fullscreen. (default nil)
   dotspacemacs-undecorated-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Show the scroll bar while scrolling. The auto hide time can be configured
   ;; by setting this variable to a number. (default t)
   dotspacemacs-scroll-bar-while-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t', `relative' or `visual' then line numbers are enabled in all
   ;; `prog-mode' and `text-mode' derivatives. If set to `relative', line
   ;; numbers are relative. If set to `visual', line numbers are also relative,
   ;; but only visual lines are counted. For example, folded lines will not be
   ;; counted and wrapped lines are counted as multiple lines.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :visual nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; When used in a plist, `visual' takes precedence over `relative'.
   ;; (default nil)
   dotspacemacs-line-numbers '(:disabled-for-modes dired-mode
                                                   doc-view-mode
                                                   markdown-mode
                                                   org-mode
                                                   pdf-view-mode
                               :size-limit-kb 1000)

   ;; Code folding method. Possible values are `evil', `origami' and `vimish'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil and `dotspacemacs-activate-smartparens-mode' is also non-nil,
   ;; `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil smartparens-mode will be enabled in programming modes.
   ;; (default t)
   dotspacemacs-activate-smartparens-mode t

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc...
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; If nil then Spacemacs uses default `frame-title-format' to avoid
   ;; performance issues, instead of calculating the frame title by
   ;; `spacemacs/title-prepare' all the time.
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Color highlight trailing whitespace in all prog-mode and text-mode derived
   ;; modes such as c++-mode, python-mode, emacs-lisp, html-mode, rst-mode etc.
   ;; (default t)
   dotspacemacs-show-trailing-whitespace t

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'changed

   ;; If non-nil activate `clean-aindent-mode' which tries to correct
   ;; virtual indentation of simple modes. This can interfere with mode specific
   ;; indent handling like has been reported for `go-mode'.
   ;; If it does deactivate it here.
   ;; (default t)
   dotspacemacs-use-clean-aindent-mode t

   ;; Accept SPC as y for prompts if non-nil. (default nil)
   dotspacemacs-use-SPC-as-y nil

   ;; If non-nil shift your number row to match the entered keyboard layout
   ;; (only in insert state). Currently supported keyboard layouts are:
   ;; `qwerty-us', `qwertz-de' and `querty-ca-fr'.
   ;; New layouts can be added in `spacemacs-editing' layer.
   ;; (default nil)
   dotspacemacs-swap-number-row nil

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil

   ;; If nil the home buffer shows the full path of agenda items
   ;; and todos. If non-nil only the file name is shown.
   dotspacemacs-home-shorten-agenda-source nil

   ;; If non-nil then byte-compile some of Spacemacs files.
   dotspacemacs-byte-compile nil))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env)
)

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."
)


(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
)


(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."
  ;; User config

  ;; Set escape keybinding to "jk", "kj"
  (setq-default
    evil-escape-key-sequence "jk"
    evil-escape-unordered-key-sequence "true")

  ;; Set the Super key as Meta key
  (setq x-super-keysym 'meta)

  ;; A more lenient centered-cursor-mode
  (setq scroll-margin 15)

  (setq-default tab-width 4)
  (setq-default c-basic-offset 4)

  (spacemacs/set-leader-keys-for-major-mode 'org-mode
    (kbd "}") 'insert-latex-graph-parentheses)
  (with-eval-after-load 'org
    (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.5))
    (define-key org-mode-map (kbd "RET") 'org-meta-return)
    )

  ;; Make evil-mode up/down operate in screen lines instead of logical lines
  (define-key evil-motion-state-map "j" 'evil-next-visual-line)
  (define-key evil-motion-state-map "k" 'evil-previous-visual-line)
  ;; Also in visual mode
  (define-key evil-visual-state-map "j" 'evil-next-visual-line)
  (define-key evil-visual-state-map "k" 'evil-previous-visual-line)

  ;; Insert State: C-j to go to next line
  (define-key evil-insert-state-map
    (kbd "C-j")
    '(lambda ()
       (interactive)
       (evil-force-normal-state)
       (evil-open-below 0)))

  ;; Insert State: C-n to go to next char
  (define-key evil-insert-state-map (kbd "C-n") 'forward-char)
  ;; Insert State: C-m to go to next `end`
  ;; From https://emacs.stackexchange.com/a/20290
  ;; (define-key input-decode-map [(control ?m)] [control-m])
  ;; (define-key evil-insert-state-map [control-m] 'evil-forward-WORD-end)

  ;; SPC w w to switch between two windows
  ;; SPC w TAB to cycle through two windows
  (spacemacs/set-leader-keys
    (kbd "w w") 'spacemacs/alternate-window
    "w TAB"   'other-window)

  ;; SPC b b to switch between two buffers (in the same window and perspective)
  ;; SPC b f to search for a buffer
  (spacemacs/set-leader-keys (kbd "bb") 'spacemacs/alternate-buffer-in-persp
                             (kbd "bf") 'ivy-switch-buffer)

  ;; g . to go back to the cursor's last position
  (define-key evil-normal-state-map (kbd "g.") 'pop-global-mark)


  ;; TODO FUCK THIS SHIT
  ;; Hack to use Tab meaningfully when autocompleting
  ;; (load "~/.emacs.d/private/local/company-simple-complete.el")
  ;; (require 'company-simple-complete)
  ;; fsharp
  ;; (setq fsharp-ac-executable "/home/tref/tmpgit/FsAutoComplete/src/FsAutoComplete/bin/Debug/net461/fsautocomplete.exe")
  ;; Fix fsharp autocomplete issue where pressing "." inserts completion
  ;; Set to "" instead of `nil` so that it still suggests stuff after a dot
  ;; (with-eval-after-load 'fsharp-mode
  ;;   (setq company-auto-complete-chars ""))
  ;; (add-hook 'fsharp-mode-hook (lambda () (setq company-auto-complete-chars "")))

  ;; Terrible workaround to avoid new indent level on pattern matching
  ;; (add-hook 'fsharp-mode-hook
  ;;   (lambda () (defconst fsharp-block-opening-re
  ;;     (concat "\\(" (mapconcat 'identity
  ;;                              '("then"
  ;;                                "else"
  ;;                                "finally"
  ;;                                "class"
  ;;                                "struct"
  ;;                                "="        ; for example: let f x =
  ;;                                "->"
  ;;                                "do"
  ;;                                "try"
  ;;                                "function")
  ;;                              "\\|")
  ;;             "\\)")
  ;;     "regular expression matching expressions which begin a block"))
  ;;   )

  ;; Delete consecutive dupes from company in case they differ by annotation only
  ;; https://github.com/company-mode/company-mode/issues/528

  (with-eval-after-load 'company
    ;; Enable 'tab and go', i.e. cycle and complete simultaneously
    (define-key company-active-map (kbd "<C-return>") 'company-complete)
    ;; (global-company-mode)
    ;; (company-tng-configure-default)

    ;; Use company-tng everywhere!
    ;; (add-hook 'company-mode-hook (lambda () (add-to-list 'company-frontends 'company-tng-frontend)))
    )
  ;; (with-eval-after-load 'company
  ;;   ;; Enable auto-completion in all buffers
  ;;   ;; Show every possible completion
  ;;   (setq company-minimum-prefix-length 1)

  ;;   (add-to-list 'company-transformers 'company-sort-prefer-same-case-prefix)
  ;;   ;; (add-to-list 'company-transformers 'delete-consecutive-dups)
  ;;   ;; (add-to-list 'company-frontends 'company-tng-frontend)
  ;;   ;; (company-tng-configure-default)

  ;;   ;; (define-key company-active-map (kbd "TAB") 'company-select-next)
  ;;   ;; (define-key company-active-map (kbd "<backtab>") 'company-select-previous)
  ;;   ;; (define-key company-active-map (kbd "RET") nil)

  ;;   (add-to-list 'company-frontends 'company-tng-frontend)
  ;;   (define-key company-active-map (kbd "TAB") 'company-select-next)
  ;;   (define-key company-active-map (kbd "<backtab>") 'company-select-previous)
  ;;   )


  ;; Csharp
  (with-eval-after-load 'csharp
    ;; Only search in relevant files
    (add-hook 'csharp-mode-hook
              (lambda ()
                (setq-local counsel-rg-base-command
                            "rg -S --no-heading --line-number --color never --type csharp --type xml %s .")))
    )

  ;; Dims unused buffers
  (use-package dimmer
    :config
    (dimmer-mode))

  (with-eval-after-load 'flycheck
    ;; Do not hide tooltips
    (setq flycheck-pos-tip-timeout 0)
    ;; Do not delay tooltips
    (setq flycheck-display-errors-delay 0)
    ;; Add flycheck to csharp
    (add-hook 'csharp-mode-hook #'flycheck-mode)

    ;; Disable flycheck in insert mode
    (add-hook 'evil-insert-state-entry-hook (lambda () (spacemacs/toggle-syntax-checking-off)))
    (add-hook 'evil-insert-state-exit-hook  (lambda () (spacemacs/toggle-syntax-checking-on)))
    )
  ;; Fsharp (F#)

  ;; Version Control and Git
  (with-eval-after-load 'git-gutter+
    ;; Set back and foreground to same color
    (set-face-background 'git-gutter+-added "green4")
    (set-face-background 'git-gutter+-deleted "red3")
    (set-face-background 'git-gutter+-modified "#2b2bff")
    (set-face-foreground 'git-gutter-fr+-added "green4")
    (set-face-foreground 'git-gutter-fr+-deleted "red3")
    (set-face-foreground 'git-gutter-fr+-modified "#2b2bff")
    (define-key evil-normal-state-map (kbd "SPC g B") 'browse-at-remote)
    )

  (with-eval-after-load 'magit
    (setq magit-blame-echo-style 'headings))

  ;;;; Haskell
  ;; (with-eval-after-load 'haskell
  ;;   (setq haskell-hoogle-command nil)
  ;;   (setq haskell-process-type 'stack-ghci)
  ;;   (setq haskell-process-path-ghci "stack")
  ;;   (setq haskell-process-args-ghci "ghci")
  ;;   ;; https://github.com/haskell/haskell-mode/issues/1553#issuecomment-397326970
  ;;   (setq haskell-process-args-stack-ghci '("--ghci-options=-ferror-spans -fshow-loaded-modules"))
  ;;   )
  ;; (spacemacs/set-leader-keys-for-major-mode 'haskell-mode (kbd "s R") 'intero-repl-eval-region)
  ;; (spacemacs/set-leader-keys-for-major-mode 'haskell-mode
  ;;   (kbd "s r")
  ;;   (defun intero-repl-eval-region-stay ()
  ;;     "Evaluate region in REPL while staying in current buffer"
  ;;     (interactive)
  ;;     (call-interactively 'intero-repl-eval-region)
  ;;     (intero-repl-switch-back)
  ;;     ))
  ;; (spacemacs/set-leader-keys-for-major-mode 'haskell-mode
  ;;   (kbd "s c")
  ;;   (defun intero-repl-clean ()
  ;;      "Clean the repl"
  ;;      (interactive)
  ;;      (call-interactively 'haskell-intero/pop-to-repl)
  ;;      (intero-repl-clear-buffer)
  ;;      (intero-repl-switch-back)
  ;;      ))
  ;; (spacemacs/set-leader-keys-for-major-mode 'haskell-mode
  ;;   (kbd "s m")
  ;;   (defun intero-repl-eval-main ()
  ;;     "Evaluate `:main' in REPL while staying in current buffer"
  ;;     (interactive)
  ;;     (intero-repl-load)
  ;;     (call-interactively 'haskell-intero/pop-to-repl)
  ;;     (call-interactively 'evil-delete-line)
  ;;     (insert ":main")
  ;;     (call-interactively 'comint-send-input)
  ;;     (intero-repl-switch-back)))

  ;; Set correct indentation
  (add-hook 'haskell-mode-hook
            (lambda ()
              (setq haskell-indentation-layout-offset 4)
              (setq haskell-indentation-left-offset 4)
              (setq haskell-indentation-starter-offset 4)
              ))

  ;;;; HIE
  (setenv "cabal_helper_libexecdir" "/home/tref/.local/bin")
  (setq lsp-haskell-process-path-hie "hie-wrapper")
  (require 'lsp-haskell)
  (add-hook 'haskell-mode-hook #'lsp)

  ;;;; Latex
  (add-hook 'doc-view-mode-hook 'auto-revert-mode)
  (spacemacs/set-leader-keys-for-major-mode 'latex-mode (kbd "o") 'TeX-next-error)
  (spacemacs/set-leader-keys-for-major-mode 'latex-mode
    (kbd "x m") 'insert-latex-graph-parentheses)
  (add-hook 'LaTeX-mode-hook
            (lambda ()
              ;; (setq evil-shift-width 4)
              ;; (setq LaTeX-indent-level 4)
              (spacemacs/set-leader-keys-for-major-mode 'latex-mode
                (kbd "b") 'build-latex-project)
              ))

  ;;;; Python
  (spacemacs/set-leader-keys-for-major-mode 'python-mode "se" 'python-evaluate-region)

  ;;;; PDF
  ;; (with-eval-after-load 'pdf-view-mode
  (add-hook 'pdf-view-mode-hook 'pdf-view-fit-page-to-window)
  ;; (add-hook 'pdf-view-mode-hook 'pdf-view-midnight-minor-mode)
  ;; )

  ;; SPC f h to search file with fzf
  (spacemacs/set-leader-keys (kbd "f h") 'fzf)

  ;;;; rg
  ;; SPC s w to search whole words with rg
  (spacemacs/set-leader-keys (kbd "s w")
    (lambda ()
      (interactive)
      (counsel-rg nil nil "-w" "rg word: ")))
  (ivy-configure 'counsel-rg
    :height 7
    ;; Search with just 1 character
    :more-chars 1)

  ;; backspace deletes indentation
  (global-hungry-delete-mode)

  ;; Use RET to go to the next field when in a snipppet
  (with-eval-after-load 'yasnippet
    (define-key yas-keymap (kbd "TAB") nil)
    (define-key yas-keymap [tab]       nil)
    (define-key yas-keymap (kbd "<return>") 'yas-next-field)
    (define-key yas-keymap (kbd "M-n")      'yas-next-field)
    (define-key yas-keymap (kbd "<C-return>") 'yas-prev-field)
    (define-key yas-keymap (kbd "M-p")        'yas-prev-field)
    )

  ;; Increase/decrease numbers
  (require 'evil-numbers)
  (global-set-key (kbd "C-+") 'evil-numbers/dec-at-pt)
  (global-set-key (kbd "C-=") 'evil-numbers/inc-at-pt)

  ;; Enable the undo tree
  (global-undo-tree-mode 1)

  ;;(global-set-key (kbd "C-/") 'dotspacemacs/)

  ;; Save with C-;
  (global-set-key (kbd "C-;") 'dotspacemacs/save-then-goto-normalmode)

  ;; Save and exit and kill window with zz
  (define-key evil-normal-state-map "zz" 'dotspacemacs/save-then-kill-window)

  ;; £ (= S-3) to go to the beginning of line
  (define-key evil-normal-state-map "£" 'evil-first-non-blank-of-visual-line)

  ;; Select all text
  (global-set-key (kbd "C-a") 'mark-whole-buffer)
  ;; C-tab to switch to other windows (like in splits)
  (global-set-key [C-tab] 'ace-window)

  ;; Multiple Cursors
  (global-evil-mc-mode 1)
  (with-eval-after-load 'evil-mc
    (evil-define-key 'normal evil-mc-key-map (kbd "C-p") 'evil-mc-skip-and-goto-prev-match
                                             (kbd "C-o") 'evil-mc-skip-and-goto-next-match
                                             (kbd "<escape>") 'evil-mc-undo-all-cursors))

  ;; Paste stuff multiple times (define-key evil-visual-state-map "p"
  ;; 'evil-paste-after-from-0)

  ;; OmniSharp server location
  (setq-default omnisharp--curl-executable-path "/usr/bin/curl")

  ;;;; Search
  ;; Ripgrep
  (evil-leader/set-key "/" 'counsel-rg)
  ;; fzf
  (evil-leader/set-key "sz" 'counsel-fzf)

  ; Disable stupid keys conflicts
  (with-eval-after-load "flyspell"
    (define-key flyspell-mode-map (kbd "C-;") nil))
  )

;;;; Custom Functions

(defun build-latex-project ()
  "Build latex project (even when not editing `main.tex')."
  (interactive)
  (if (string= (buffer-name) "main.tex")
      (call-interactively 'latex/build)
    (progn
      (switch-to-buffer "main.tex")
      (call-interactively 'latex/build)
      (switch-to-prev-buffer))))

(defun insert-latex-graph-parentheses (start end)
  "Insert LateX math delimiters \\{\\} at cursor point, or wrap region with them."
  (interactive "r")
  (if (eq evil-state 'visual)
      (save-excursion
        (goto-char end) (insert "\\\}")
        (goto-char start) (insert "\\\{"))
    (progn
      (forward-char 1)
      (insert "\\\{\\\}")
      (backward-char 2)
      (evil-insert-state)))
  )

(defun python-evaluate-region (start end)
  "Evaluate the current selected region in the python repl."
  (interactive "r")
  (comint-send-region "*Python*" start end)
  (switch-to-buffer "*Python*")
  (comint-send-input)
  (previous-buffer)
  )


(defun dotspacemacs/save-then-goto-normalmode ()
  "Save the current buffer then go to normal mode"
  (interactive)
  (save-buffer)
  (evil-force-normal-state))

(defun dotspacemacs/save-then-kill-window ()
  "Save the current buffer then kill the buffer and window"
  (interactive)
  (save-buffer)
  (kill-buffer-and-window))


;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-indent-environment-list
   '(("verbatim" current-indentation)
     ("verbatim*" current-indentation)
     ("filecontents" current-indentation)
     ("filecontents*" current-indentation)
     ("tabular")
     ("tabular*")
     ("align")
     ("align*")
     ("array" LaTeX-indent-tabular)
     ("eqnarray" LaTeX-indent-tabular)
     ("eqnarray*" LaTeX-indent-tabular)
     ("displaymath")
     ("equation")
     ("equation*")
     ("picture")
     ("tabbing")))
 '(evil-shift-width 4)
 '(evil-want-Y-yank-to-eol nil)
 '(haskell-hoogle-command "hoogle --info")
 '(package-selected-packages
   '(add-node-modules-path import-js grizzl typescript-mode dap-mode bui yasnippet-snippets yapfify yaml-mode xterm-color ws-butler writeroom-mode winum which-key wgrep web-mode web-beautify vterm volatile-highlights vi-tilde-fringe uuidgen use-package undo-tree treemacs-projectile treemacs-persp treemacs-magit treemacs-icons-dired treemacs-evil toc-org terminal-here tagedit symon symbol-overlay string-inflection string-edit sphinx-doc spaceline-all-the-icons smex smeargle slim-mode shell-pop scss-mode sass-mode restart-emacs rainbow-delimiters quickrun pytest pyenv-mode py-isort pug-mode prettier-js popwin poetry pippel pipenv pip-requirements pcre2el password-generator paradox overseer orgit-forge org-superstar org-rich-yank org-projectile org-present org-pomodoro org-mime org-download org-cliplink org-brain open-junk-file omnisharp npm-mode nose nodejs-repl nameless multi-term multi-line mmm-mode markdown-toc magit-section macrostep lsp-ui lsp-treemacs lsp-python-ms lsp-pyright lsp-origami lsp-latex lsp-ivy lsp-haskell lorem-ipsum livid-mode live-py-mode link-hint json-navigator json-mode js2-refactor js-doc ivy-yasnippet ivy-xref ivy-rtags ivy-purpose ivy-hydra ivy-avy indent-guide importmagic impatient-mode hybrid-mode hungry-delete hlint-refactor hl-todo hindent highlight-parentheses highlight-numbers highlight-indentation helm-make haskell-snippets google-translate google-c-style golden-ratio gnuplot gitignore-templates gitignore-mode gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link git-gutter-fringe+ gh-md ggtags fzf fuzzy futhark-mode fsharp-mode font-lock+ flyspell-correct-ivy flycheck-ycmd flycheck-rtags flycheck-pos-tip flycheck-package flycheck-haskell flycheck-elsa flx-ido fancy-battery eyebrowse expand-region evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-textobj-line evil-surround evil-org evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-lisp-state evil-lion evil-indent-plus evil-iedit-state evil-goggles evil-exchange evil-escape evil-ediff evil-easymotion evil-collection evil-cleverparens evil-args evil-anzu eshell-z eshell-prompt-extras esh-help emr emmet-mode elisp-slime-nav editorconfig dumb-jump drag-stuff dotenv-mode disaster dired-quick-sort dimmer diminish devdocs define-word dante cython-mode csv-mode cpp-auto-include counsel-projectile counsel-gtags counsel-css company-ycmd company-web company-statistics company-rtags company-reftex company-quickhelp company-math company-lua company-cabal company-c-headers company-auctex company-anaconda column-enforce-mode cmm-mode clojure-snippets clean-aindent-mode cider-eval-sexp-fu cider centered-cursor-mode ccls browse-at-remote boogie-friends blacken auto-yasnippet auto-highlight-symbol auto-dictionary auto-compile auctex-latexmk attrap aggressive-indent ace-link ac-ispell))
 '(vc-follow-symlinks t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(highlight-parentheses-highlight ((nil (:weight ultra-bold))) t))
)
