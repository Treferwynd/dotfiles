" All system-wide defaults are set in $VIMRUNTIME/archlinux.vim (usually just
" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

"" Use true colors
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

set showcmd         " Show (partial) command in status line.

set number          " Show line numbers.

set hlsearch        " Highlight all its matches
set incsearch       " Search as you type
set ignorecase      " Ignore case in search patterns
set smartcase       " Case sensitive if search contains upper case characters

set backspace=2     " Influences the working of <BS>, <Del>, CTRL-W
                    " and CTRL-U in Insert mode. This is a list of items,
                    " separated by commas. Each item allows a way to backspace
                    " over something.

set formatoptions=c,q,r,t " This is a sequence of letters which describes how
                    " automatic formatting is to be done.
                    "
                    " letter    meaning when present in 'formatoptions'
                    " ------    ---------------------------------------
                    " c         Auto-wrap comments using textwidth, inserting
                    "           the current comment leader automatically.
                    " q         Allow formatting of comments with "gq".
                    " r         Automatically insert the current comment leader
                    "           after hitting <Enter> in Insert mode.
                    " t         Auto-wrap text using textwidth (does not apply
                    "           to comments)

set background=dark " When set to "dark", Vim will try to use colors that look
                    " good on a dark background. When set to "light", Vim will
                    " try to use colors that look good on a light background.
                    " Any other value is illegal.

set mouse=a         " Enable the use of the mouse.

set clipboard=unnamedplus   " Use system clipboard

set autoindent      " Copy indent from current line when starting a new line
set expandtab       " Use spaces instead of tabs
set tabstop=4       " Number of spaces that a <Tab> in the file counts for.
set shiftwidth=4    " Number of spaces to use for each step of (auto)indent.

set modeline        " Use modelines

set nowildmenu      " NOT Show line with all possible completions for : commands

set wildmode=list:longest,full   " Cycle through : commands completions

set scrolloff=8     " Keep 3 lines below and above the cursor while scrolling
set autoread         "Reload files changed outside vim

"" Backup and swap directories
set backupdir=$HOME/.vim/backupfiles,/tmp
set directory=$HOME/.vim/swapfiles//,/tmp
"" Persistent undo file
if has('persistent_undo')
    if isdirectory(expand('$HOME/.vim'))
        let my_undo_dir = expand('$HOME/.vim/undofiles')
    else
        let my_undo_dir = '/tmp/vim_undofiles'           " Use /tmp as fallback
    endif
    silent call system('mkdir ' . my_undo_dir)
    let &undodir=my_undo_dir
    set undofile
endif

"" Highlight matching parentesis
au VimEnter * DoMatchParen

"" Indent and manual folding
augroup vimrc
    au BufReadPre * setlocal foldmethod=indent
    au BufWinEnter * if &fdm == 'indent' | setlocal foldmethod=manual | endif
augroup END
" Folds open by default
set nofoldenable

" Reload sysinit.vim
nmap <Leader>r :source $VIM/sysinit.vim

" Needed by remote plugins (i.e. Deoplete) after upates
function! DoRemote(arg)
    UpdateRemotePlugins
endfunction

"""" PLUGINS
" Manage plugins with vim-plug
call plug#begin()
    " For everybodyz
    " TODO: for gitgutter check if lazy load actually works
    Plug 'ctrlpvim/ctrlp.vim'                 " Open dem files ez
    Plug 'junegunn/vim-easy-align'            " Align dem stuff
    Plug 'matze/vim-move'                     " Move Lines
    Plug 'nanotech/jellybeans.vim'            " Jellybeans theme
    Plug 'rhysd/clever-f.vim'                 " Use f/F to automatically repeat search
    Plug 'terryma/vim-multiple-cursors'       " Multiple cursors
    Plug 'tpope/vim-commentary'               " Un/comment with C-/
    Plug 'triglav/vim-visual-increment'       " C-a and C-x to increase/decrease number and letters

    let g:installed_gitgutter = 1             " Git stuff in vim
    Plug 'airblade/vim-gitgutter'

    " On demand
    Plug 'scrooloose/nerdtree',               { 'on': 'NERDTreeToggle' }             " Show file browser
    Plug 'vim-scripts/taglist.vim',           { 'on': ['TlistOpen', 'TlistToggle'] } " Taglist source code browser

    "" Questor Specific
    if hostname() == "questor"
        Plug 'vim-airline/vim-airline'        " Cool status line
        Plug 'vim-airline/vim-airline-themes' " Themes for the cool status line
        Plug 'kana/vim-textobj-user'          " Custom text objects
        Plug 'beloglazov/vim-textobj-quotes'  " Use 'q' as quotes text objects ("", '' and ``)
        Plug 'Julian/vim-textobj-brace'       " Use 'j' for braces text objects ([], {} and ())
        let g:installed_incsearch = 1         " Highlights all search results while typing
        Plug 'haya14busa/incsearch.vim'
        Plug 'neomake/neomake'                " Linting???
        Plug 'kshenoy/vim-signature'          " Cool markers
        Plug 'ntpeters/vim-airline-colornum'  " Highlight current line number
        Plug 'ntpeters/vim-better-whitespace' " Better whitespace highlighting for Vim
                                              " Code completion engine
        Plug 'Shougo/deoplete.nvim',          { 'do': function('DoRemote') }
        Plug 'Shougo/neopairs.vim'            " Auto insert parentheses
        " Plug 'cohama/lexima.vim'              " Auto insert parentheses
        Plug 'tpope/vim-fugitive'             " Git wrapper
        Plug 'tpope/vim-surround'             " Surround stuff with stuff
        " Plug 'Yggdroot/indentLine'          " Show indent lines
        " Plug 'Yggdroot/hiPairs'             " Highlight matching parentheses, right now is super slow

                                              " Code snippets
        Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

        Plug 'ryanoasis/vim-devicons'         " Cool icons, should be loaded as late as possible

        " On demand
                                              " Taskwarrior directly inside vim
        Plug 'farseer90718/vim-taskwarrior',  { 'on': 'TW' }

        " Syntax
        Plug 'Firef0x/PKGBUILD.vim',          { 'for': 'PKGBUILD' }
        Plug 'mxw/vim-jsx',                   { 'for': 'javascript.jsx' }
        Plug 'pangloss/vim-javascript',       { 'for': 'javascript' }
        Plug 'PotatoesMaster/i3-vim-syntax',  { 'for': 'i3' }

        " Code completion and stuff
        Plug 'artur-shaik/vim-javacomplete2', { 'for': 'java' }

        Plug 'tpope/vim-classpath',           { 'for': 'clojure' }
        Plug 'tpope/vim-fireplace',           { 'for': 'clojure' }
        Plug 'vim-scripts/paredit.vim',       { 'for': 'clojure' }

        "
        " Plug 'Rip-Rip/clang_complete',        { 'for': ['c', 'cpp'] }
        Plug 'zchee/deoplete-clang',          { 'for': ['c', 'cpp'] }
        Plug 'zchee/deoplete-jedi',           { 'for': 'python' }
        Plug 'zchee/deoplete-go',             { 'for': 'go', 'do': 'make' }

        Plug 'rust-lang/rust.vim',            { 'for': 'rust'}
        Plug 'racer-rust/vim-racer',          { 'for': 'rust'}

        " Check syntax
        Plug 'scrooloose/syntastic'
    endif

call plug#end()

"" Deoplete
let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_completion_start_length = 0
" Enable snippets with short names
call deoplete#custom#set('ultisnips', 'min_pattern_length', 0)

"" C++
" deoplete-clang opions
let g:deoplete#sources#clang#libclang_path = "/usr/lib/libclang.so"
let g:deoplete#sources#clang#clang_header ="/usr/include/clang/"

"" C#
let g:clang_complete_auto = 0
let g:clang_auto_select = 0
" let g:clang_close_preview = 1
let g:clang_omnicppcomplete_compliance = 0
let g:clang_make_default_keymappings = 0
let g:clang_use_library = 1

"" Java completion
let g:ultisnips_java_brace_style="nl"
autocmd FileType java set omnifunc=javacomplete#Complete
" Control Space to force completion
autocmd FileType java inoremap <C-Space> <C-x><C-o>

"" Python
set omnifunc=jedi#completions
let g:jedi#auto_initialization = 1
let g:jedi#auto_vim_configuration = 0
let g:jedi#popup_select_first = 0
let g:jedi#completions_enabled = 0
let g:jedi#force_py_version = 3
let g:jedi#smart_auto_mappings = 0
let g:jedi#show_call_signatures = 0
let g:jedi#max_doc_height = 3
let g:jedi#auto_close_doc = 1

"" Rust
set hidden
let g:racer_cmd = "/usr/bin/racer"
let $RUST_SRC_PATH="/usr/src/rust/src/"


"" Airline
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#hunks#enabled = 1
let g:airline#extensions#branch#enabled = 1

"" airline-colornum
let g:airline_colornum_enabled = 1
set cursorline
hi clear CursorLine

"" clever-f
let g:clever_f_smart_case = 1

"" easy-align
" Start interactive EasyAlign in visual mode (e.g. vip<Enter>)
vmap <Enter> <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

"" Task
" Default view
let g:task_report_command  = ['simple']
let g:task_report_name = 'simple'
" default fields to ask when adding a new task
let g:task_default_prompt  = ['description', 'project']

"" Ultisnips
" Use custom scripts
let g:UltiSnipsSnippetDirectories=["UltiSnips", "my-snippets"]
" Make tab behave as it should
let g:UltiSnipsExpandTrigger = "<nop>"
let g:ulti_expand_or_jump_res = 0
function ExpandSnippetOrCarriageReturn()
    let snippet = UltiSnips#ExpandSnippetOrJump()
    if g:ulti_expand_or_jump_res > 0
        return snippet
    else
        return "\<CR>"
    endif
endfunction

inoremap <expr> <CR> pumvisible() ? "<C-R>=ExpandSnippetOrCarriageReturn()<CR>" : "\<CR>"
""" TODO: this is probably a better alternative:
"" http://stackoverflow.com/a/18685821
" To enalbe ctrl-k shortcut
inoremap <c-x><c-k> <c-x><c-k>
" Cycle trough placeholders
let g:UltiSnipsJumpForwardTrigger="<cr>"
let g:UltiSnipsJumpBackwardTrigger="<c-l>"

"" Taglist plugin
let Tlist_Compact_Format = 1
let Tlist_GainFocus_On_ToggleOpen = 1
"let Tlist_Close_On_Select = 1

"" Commentary to ctrl-7 and ctrl-/
imap <C-_> <Esc>gcc
nmap <C-_> gcc
vmap <C-_> gc

"" Incremental Search "
" Check if incsearch is installed (or just loaded)
if exists('g:installed_incsearch')
    map /  <Plug>(incsearch-forward)
    map ?  <Plug>(incsearch-backward)
    map g/ <Plug>(incsearch-stay)

    " Automatic no highlight
    set hlsearch
    let g:incsearch#auto_nohlsearch = 1
    map n  <Plug>(incsearch-nohl-n)
    map N  <Plug>(incsearch-nohl-N)
    map *  <Plug>(incsearch-nohl-*)
    map #  <Plug>(incsearch-nohl-#)
    map g* <Plug>(incsearch-nohl-g*)
    map g# <Plug>(incsearch-nohl-g#)
endif

"" GitGutter
" Check if GitGutter is installed (or just loaded)
let g:gitgutter_enabled = 1
let g:gitgutter_signs = 1
let g:gitgutter_highlight_lines = 0

if exists('g:installed_gitgutter')
    " Use n/N to navigate through the changed hunks
    function! ToggleGitGutting()
        GitGutterLineHighlightsToggle
        if g:gitgutter_highlight_lines
            nmap n <Plug>GitGutterNextHunk
            nmap N <Plug>GitGutterPrevHunk
        else
            unmap n
            unmap N
        endif
    endfunction

    inoremap <F5> <Esc>:GitGutterToggle<CR>a
    nnoremap <F5> :GitGutterToggle<CR>
    inoremap <F6> <Esc>:call ToggleGitGutting()<CR>a
    nnoremap <F6> :call ToggleGitGutting()<CR>
endif

"" vim-neomake
autocmd! BufWritePost * Neomake

"" vim-visual-increment
" Increment also letters, octal and hex!
set nrformats=octal,hex

"" vim-move
" Move lines and blocks of text!
let g:move_map_keys = 0
vmap <C-k> <Plug>MoveBlockDown
vmap <C-l> <Plug>MoveBlockUp
nmap <C-k> <Plug>MoveLineDown
nmap <C-l> <Plug>MoveLineUp

""""""""""""
" MAPPINGS "
""""""""""""

" Lots of people remapped <Space> to <Leader>
let mapleader=","
noremap <Space> :

" Quicker start/end of line
noremap H ^
noremap L $

" Paste on new line with è:
noremap è :pu<CR>

" Move current line down
function MoveLineDown()
    :s/^/\r/g
    :noh
endfunction
noremap + :call MoveLineDown()<CR>

"" Spell checker
" Use n/N to navigate through the changed hunks
function! ToggleSpellCheck()
    let &spell = !&spell
    if &spell
        map n ]s
        map N [s
    else
        unmap n
        unmap N
    endif
endfunction
noremap <F10> :call ToggleSpellCheck()<CR>

inoremap <Leader>l <Esc>:TlistToggle<CR>
nnoremap <Leader>l :TlistToggle<CR>
" inoremap <C-s-l> <Esc>:TlistToggle<CR>
" nnoremap <C-s-l> :TlistToggle<CR>
" Control-s to save
inoremap <C-s> <Esc>:w<CR>
nnoremap <C-s> :w<CR>
" Control-w to save
"inoremap <C-w> <Esc>:w<CR>
"nnoremap <C-w> :w<CR>
" Save and run
inoremap <F9> <Esc>:w<CR>:!%:p<CR>
nnoremap <F9> :w<CR>:!%:p<CR>

" Fast window resizing with +/- keys (horizontal); / and * keys (vertical)
if bufwinnr(1)
    map <kPlus> <C-W>+
    map <kMinus> <C-W>-
    map <kDivide> <c-w><
    map <kMultiply> <c-w>>
endif
" Toogle NERDTree
inoremap <C-o> <Esc>:NERDTreeToggle<CR>
nnoremap <C-o> :NERDTreeToggle<CR>
" Mmmmmhhhh
" Control-q to quit
inoremap <C-q> <Esc>:q<CR>
nnoremap <C-q> :q<CR>
" Control-q to quit
inoremap <C-Q> <Esc>:q!<CR>
nnoremap <C-Q> :q!<CR>

"" Keys
" Move inside wrapped lines
noremap j gj
noremap k gk

" jk to esc
imap jk <Esc>
" jj or kj to go to next line
imap jj <Esc>o
imap kj <Esc>o

" Tab to manage indentation
nnoremap <Tab> >>
nnoremap <S-Tab> <<
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv
" Cycle through completions or use Tab normally if none available
inoremap <expr> <Tab> pumvisible() ? "<C-n>" : "<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "<C-p>" : "<C-d>"

" Y to yank to end of line
map Y y$

"""""""""""""""""
" Custom Colors "
"""""""""""""""""
" 80 char limit
augroup highlight_long_lines
    autocmd BufEnter * highlight OverLength ctermbg=235
    autocmd BufEnter * match OverLength /\%79v.\+/
augroup END

" TODO labels
if !exists("autocmd_colorscheme_loaded")
    let autocmd_colorscheme_loaded = 1
    autocmd ColorScheme * highlight Todo       ctermbg=64    ctermfg=225      cterm=bold
    autocmd ColorScheme * highlight TodoMild   ctermbg=208   ctermfg=225      cterm=bold
    autocmd ColorScheme * highlight TodoCrit   ctermbg=88    ctermfg=White    cterm=bold
    autocmd ColorScheme * highlight Info       ctermbg=27    ctermfg=White    cterm=bold
endif

" if has("autocmd")
"   " Highlight TODO, TODO2, TODO3, NOTE, etc.
"   if v:version > 701
"   	autocmd Syntax * call matchadd('Todo',     '\W\zs\(TODO1:\|TODO:\|WIP:\|TEST:\)')
"   	autocmd Syntax * call matchadd('Todo',     '\W\zs\(TODO1\|TODO\|WIP\|TEST\)')
"   	autocmd Syntax * call matchadd('TodoMild', '\W\zs\(TODO2:\|DEBUG:\|HACK:\)')
"   	autocmd Syntax * call matchadd('TodoMild', '\W\zs\(TODO2\|DEBUG\|HACK\)')
"   	autocmd Syntax * call matchadd('TodoCrit', '\W\zs\(TODO3:\|BUG:\|FIXME:\)')
"   	autocmd Syntax * call matchadd('TodoCrit', '\W\zs\(TODO3\|BUG\|FIXME\)')
"   	autocmd Syntax * call matchadd('Info',     '\W\zs\(NOTE:\|AXIOMS:\|AXIOM:\|IDEA:\|INFO:\|N\.B\.:\|NB:\)')
"   	autocmd Syntax * call matchadd('Info',     '\W\zs\(NOTE\|AXIOMS\|AXIOM\|IDEA\|INFO\|N\.B\.\|NB\)')
"   endif
"endif

"""""""""
" Theme "
"""""""""
"let g:molokai_original = 1
colo jellybeans

" Visualize indentation lines with tabs
set list
set listchars=tab:\│\ ,extends:>,precedes:<
"set listchars=eol:¬,
hi SpecialKey ctermbg=233 ctermfg=234
" Visualize indentation lines with spaces
let g:indentLine_color_term = 234
let g:indentLine_char = '│'
let g:indentLine_enabled = 1

" Navigate more easily in vimdiff
if &diff
    noremap <space> ]cz.
    noremap n ]cz.
    noremap N [cz.
endif

"""""""""""""
" File type "
"""""""""""""
autocmd BufReadPost PKGBUILD  set filetype=PKGBUILD
autocmd FileType c   setlocal tabstop=4 shiftwidth=4 noexpandtab
autocmd FileType cs  setlocal tabstop=4 shiftwidth=4 noexpandtab
autocmd FileType go  setlocal tabstop=4 shiftwidth=4 noexpandtab
au BufRead,BufNewFile *.dfg set filetype=dfg
