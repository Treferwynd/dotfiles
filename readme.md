# Dotfiles
Use this as a remainder, not as a script
```sh
ln -s dotfiles/bashrc ~/.bashrc
ln -s dotfiles/bash_profile ~/.bash_profile
ln -s dotfiles/gtkrc-2.0.mine ~/.gtkrc-2.0.mine
ln -s dotfiles/transmission-daemon ~/.config/transmission-daemon/settings.json

ln -s ~/dotfiles/config/check_episodes/settings.json ~/.config/check_episodes/settings.json
ln -s ~/dotfiles/config/git/ignore ~/.config/git/ignore
ln -s ~/dotfiles/config/i3/config ~/.config/i3/config
ln -s ~/dotfiles/config/termite/config ~/.config/termite/config

ln -s /home/tref/dotfiles/inputrc /etc/inputrc
ln -s /home/tref/dotfiles/nanorc /etc/nanorc
ln -s /home/tref/dotfiles/vimrc /etc/vimrc
ln -s /home/tref/dotfiles/screenrc /etc/screenrc
ln -s /home/tref/dotfiles/root_bashrc /root/.bashrc
```
